# Hadolint

Hadolint is a tool to lint Dockerfiles. From it's documentation:
`A smarter Dockerfile linter that helps you build best practice Docker images`.

Upstream documentation can be found [here](https://github.com/hadolint/hadolint).
