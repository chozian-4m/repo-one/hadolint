ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8-minimal
ARG BASE_TAG=8.4

FROM hadolint/hadolint:v2.10.0 AS base

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

RUN microdnf update --nodocs && \
  microdnf clean all && \
  rm -rf /var/yum/cache

COPY --from=base /bin/hadolint /usr/bin/hadolint
USER 1000
HEALTHCHECK NONE
ENTRYPOINT ["hadolint"]
